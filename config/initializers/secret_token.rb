# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Treebook::Application.config.secret_key_base = '4e5add4d2cd1830a0074d544696cb19278ac30596dc5906cd60118c3b4097bb7890af8d99f184486c192b59f845a769a5d013e979abbe18d1a758b9d7d61a863'
